﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;

namespace NMEA_Utils
{
    /// <summary>
    /// Class that encapsulates a NMEA sentence.
    /// </summary>
    public abstract class NMEA_Sentence
    {
        /// <summary>
        /// Constructor initializes member variables.
        /// </summary>
        public NMEA_Sentence()
        {
            mHeader = "N/A";
            mChecksum = "N/A";
            mReceivedPackets = 0;
        }

        /// <summary>
        /// Method to call when subscribers need to be told about an Error.
        /// </summary>
        /// <param name="message"></param>
        public void LogError(string message)
        {
            ErrorHandler.Instance().LogError(message);
        }

        /// <summary>
        /// Decodes the passed in NMEA sentence.
        /// </summary>
        /// <param name="nmea"></param>
        public abstract void Parse(string[] nmea);

        /// <summary>
        /// Checks if the header is correct.
        /// </summary>
        /// <param name="header"></param>
        /// <returns>True if the header is correct.</returns>
        protected bool IsHeaderValid(string header)
        {
            if (mHeader != header)
            {
                LogError("Header Mismatch: Expected " + mHeader + "but Received " + mHeader);
                return false;
            }

            // We've received a valid packet
            mReceivedPackets++;

            return true;
        }

        /// <summary>
        /// Unpacks the checksum and strips the <CR><LF> from the end.
        /// </summary>
        /// <param name="checksum"></param>
        protected void ParseChecksum(string checksum)
        {
            if (checksum == "")
            {
                LogError("Checksum Error: " + mHeader + " checksum is empty!");
                return;
            }

            mChecksum = checksum.Substring(0, 2);
        }

        /// <summary>
        /// Returns the NMEA header of this sentence.
        /// </summary>
        public string Header
        {
            get { return mHeader; }
        }

        /// <summary>
        /// Returns the checksum of this sentence.
        /// </summary>
        public string Checksum
        {
            get { return mChecksum; }
        }

        /// <summary>
        /// Returns the number of parsed sentences.
        /// </summary>
        public int ReceivedPackets
        {
            get { return mReceivedPackets; }
            set { mReceivedPackets = value; }
        }

        protected string mHeader;
        protected string mChecksum;
        protected int mReceivedPackets;
    }
}
