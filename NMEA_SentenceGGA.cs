﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NMEA_Utils
{
    /// <summary>
    /// GGA - Global Positioning System Fix Data
    ///     Time, position and fix related data for a GPS receiver.
    /// Structure:
    ///     $GPGGA,hhmmss.sss,ddmm.mmmm,a,dddmm.mmmm,a,x,xx,x.x,x.x,M,,,,xxxx*hh<CR><LF>
    /// </summary>
    public class NMEA_SentenceGGA : NMEA_SentenceWithPositionAndUtcTime
    {
        public NMEA_SentenceGGA()
        {
            mHeader = "$GPGGA";
            mGpsQuality = "N/A";
            mSatellitesUsed = 0;
            mHDOP = 0.0;
            mAltitude_ft = 0.0;
            mDGPS_StationID = "0000";
        }

        public override void Parse(string[] nmea)
        {
            if (!IsHeaderValid(nmea[0]))
            {
                return;
            }

            mUtcTime = NMEA_Handler.ParseUtcTime(nmea[1]);
            mLatitudeD = NMEA_Handler.ParseLatitudeD(nmea[2] + nmea[3]);
            mLongitudeD = NMEA_Handler.ParseLongitudeD(nmea[4] + nmea[5]);
            mLatitudeDM = NMEA_Handler.ParseLatitudeDM(nmea[2] + nmea[3]);
            mLongitudeDM = NMEA_Handler.ParseLongitudeDM(nmea[4] + nmea[5]);
            mLatitudeDMS = NMEA_Handler.ParseLatitudeDMS(nmea[2] + nmea[3]);
            mLongitudeDMS = NMEA_Handler.ParseLongitudeDMS(nmea[4] + nmea[5]);
            mLatitude_deg = NMEA_Handler.ParseLatitudeDeg(nmea[2], nmea[3]);
            mLongitude_deg = NMEA_Handler.ParseLongitudeDeg(nmea[4], nmea[5]);
            mGpsQuality = ParseGpsQuality(nmea[6]);
            mSatellitesUsed = int.Parse(nmea[7]);
            mHDOP = double.Parse(nmea[8]);
            mAltitude_ft = double.Parse(nmea[9]);
            //nmea[10] == "M";  // for Meters
            //nmea[11] == heightOfGeoidAboveEllipsoid_m;
            //nmea[12] == "M";  // for Meters
            //nmea[13] == secondsSincelastDgpsUpdate;
            mDGPS_StationID = nmea[14];

            ParseChecksum(nmea[15]);
        }

        private string ParseGpsQuality(string quality)
        {
            switch (int.Parse(quality))
            {
                case 0: mGpsQuality = "Position fix unavailable";   break;
                case 1: mGpsQuality = "Valid position fix, SPS mode";   break;
                case 2: mGpsQuality = "Valid position fix, differential GPS mode";   break;
                case 3: mGpsQuality = "GPS PPS Mode, fix valid";   break;
                case 4: mGpsQuality = "Real Time Kinematic. System used in RTK mode with fixed integers";   break;
                case 5: mGpsQuality = "Float RTK. Satellite system used in RTK mode. Floating integers";   break;
                case 6: mGpsQuality = "Estimated (dead reckoning) Mode";   break;
                case 7: mGpsQuality = "Manual Input Mode";   break;
                case 8: mGpsQuality = "Simulator Mode"; break;
                default: mGpsQuality = "Invalid Code";  break;
            }

            return mGpsQuality;
        }

        /// <summary>
        /// Returns the GPS Quality type.
        /// </summary>
        public string GpsQuality
        {
            get { return mGpsQuality; }
        }

        /// <summary>
        /// Returns the number of tracked satellites.
        /// </summary>
        public int SatellitesUsed
        {
            get { return mSatellitesUsed; }
        }

        /// <summary>
        /// Returns the Horizontal Dilution of Precision.
        /// </summary>
        public double HDOP
        {
            get { return mHDOP; }
        }

        /// <summary>
        /// Returns the GPS altitude in feet.
        /// </summary>
        public double Altitude_Ft
        {
            get { return mAltitude_ft; }
        }

        /// <summary>
        /// Returns the Differential GPS Reference Station ID.  Null when DGPS not used.
        /// </summary>
        public string DGPS_StationID
        {
            get { return mDGPS_StationID; }
        }

        private string mGpsQuality;
        private int mSatellitesUsed;
        private double mHDOP;
        private double mAltitude_ft;
        private string mDGPS_StationID;
    }
}
