﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NMEA_Utils
{
    public class NMEA_SentenceGSA : NMEA_Sentence
    {
        /// <summary>
        /// GSA – GNSS DOP and Active Satellites
        ///     GPS receiver operating mode, satellites used in the navigation 
        ///     solution reported by the GGA or GNS sentence and DOP values.
        /// Structure:
        ///     $GPGSA,A,x,xx,xx,xx,xx,xx,xx,xx,xx,xx,xx,xx,xx,x.x,x.x,x.x*hh<CR><LF></summary>
        public NMEA_SentenceGSA()
        {
            mHeader = "$GPGSA";
            mFixMode = "N/A";
            mFixType = "N/A";
            mSatellitesUsed = new string[12];
            mPDOP = 0.0;
            mHDOP = 0.0;
            mVDOP = 0.0;
        }

        public override void Parse(string[] nmea)
        {
            if (!IsHeaderValid(nmea[0]))
            {
                return;
            }

            int index = 1;
            int satIndex = 0;

            mFixMode = ParseFixMode(nmea[index++]);
            mFixType = ParseFixType(nmea[index++]);

            // Now handle the satellites (up to 12 integers)
            // We're done when we find a decimal
            for (; index < 15; index++)
            {
                mSatellitesUsed[satIndex++] = nmea[index];
            }

            mPDOP = double.Parse(nmea[index++]);
            mHDOP = double.Parse(nmea[index++]);
            mVDOP = double.Parse(nmea[index++]);

            ParseChecksum(nmea[index++]);
        }

        private string ParseFixMode(string fixMode)
        {
            switch (fixMode)
            {
                case "M": mFixMode = "Manual"; break;
                case "A": mFixMode = "Automatic"; break;
                default: mFixMode = "Invalid Code"; break;
            }

            return mFixMode;
        }

        private string ParseFixType(string fixType)
        {
            switch (int.Parse(fixType))
            {
                case 1: mFixType = "Fix not available"; break;
                case 2: mFixType = "2D"; break;
                case 3: mFixType = "3D"; break;
                default: mFixType = "Invalid Code"; break;
            }

            return mFixType;
        }

        /// <summary>
        /// Returns the tracking mode: 
        ///     M - Manual, force to operate in 2D or 3D mode.
        ///     A - Automatic, allowed to automatically switch 2D/3D.
        /// </summary>
        public string FixMode
        {
            get { return mFixMode; }
        }

        /// <summary>
        /// Returns the GPS Fix Type: "Fix not available", "2D", "3D"
        /// </summary>
        public string FixType
        {
            get { return mFixType; }
        }

        /// <summary>
        /// Returns an array of ID numbers of satellites used in the solution.
        /// </summary>
        public string[] SatellitesUsed
        {
            get { return mSatellitesUsed; }
        }

        /// <summary>
        /// Returns the Position Dilution of Precision (00.0 to 99.9).
        /// </summary>
        public double PDOP
        {
            get { return mPDOP; }
        }

        /// <summary>
        /// Returns the Horizontal Dilution of Precision (00.0 to 99.9).
        /// </summary>
        public double HDOP
        {
            get { return mHDOP; }
        }

        /// <summary>
        /// Returns the Vertical Dilution of Precision (00.0 to 99.9).
        /// </summary>
        public double VDOP
        {
            get { return mVDOP; }
        }

        private string mFixMode;
        private string mFixType;
        private string[] mSatellitesUsed;
        private double mPDOP;
        private double mHDOP;
        private double mVDOP;
    }
}
