﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NMEA_Utils
{
    /// <summary>
    /// GLL – Latitude/Longitude
    ///     Latitude and longitude of current position, time, and status.
    /// Structure:
    ///     $GPGLL,ddmm.mmmm,a,dddmm.mmmm,a,hhmmss.sss,A,a*hh<CR><LF>
    /// </summary>
    public class NMEA_SentenceGLL : NMEA_SentenceWithPositionAndUtcTime
    {
        public NMEA_SentenceGLL()
        {
            mHeader = "$GPGLL";
            mStatus = "N/A";
            mModeIndicator = "N/A";
        }

        // TODO: Implement this method
        public override void Parse(string[] nmea)
        {
            if (!IsHeaderValid(nmea[0]))
            {
                return;
            }

            mLatitude_deg = NMEA_Handler.ParseLatitudeDeg(nmea[1], nmea[2]);
            mLongitude_deg = NMEA_Handler.ParseLongitudeDeg(nmea[3], nmea[4]);
            mUtcTime = NMEA_Handler.ParseUtcTime(nmea[5]);
            mStatus = ParseStatus(nmea[6]);
            mModeIndicator = NMEA_Handler.ParseModeIndicator(nmea[7]);

            ParseChecksum(nmea[8]);
        }

        private string ParseStatus(string status)
        {
            switch (status)
            {
                case "A": mStatus = "Data valid"; break;
                case "V": mStatus = "Data not valid"; break;
                default: mStatus = "Invalid Code"; break;
            }

            return mStatus;
        }

        /// <summary>
        /// Returns the current GPS status.  Possible values are:
        ///     Data Valid
        ///     Data Not Valid
        /// </summary>
        public string Status
        {
            get { return mStatus; }
        }

        /// <summary>
        /// Returns the GPS mode.  Possible values are:
        ///     Not valid
        ///     Autonomous mode
        ///     Differential mode
        ///     Estimated (dead reckoning) mode
        ///     Manual input mode
        ///     Simulator mode
        /// </summary>
        public string ModeIndicator
        {
            get { return mModeIndicator; }
        }

        private string mStatus;
        private string mModeIndicator;
    }
}
