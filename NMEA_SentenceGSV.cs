﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NMEA_Utils
{
    /// <summary>
    /// GSV – GNSS Satellites in View
    ///     Number of satellites (SV) in view, satellite ID numbers, elevation,
    ///     azimuth, and SNR value. Four satellites maximum per transmission.
    /// Structure:
    ///     $GPGSV,x,x,xx,xx,xx,xxx,xx,…,xx,xx,xxx,xx *hh<CR><LF></summary>
    public class NMEA_SentenceGSV : NMEA_Sentence
    {
        public struct sSat
        {
            public int mID;
            public int mElevation_deg;
            public int mAzimuth_deg;
            public int mSNR;
        }

        public NMEA_SentenceGSV()
        {
            mHeader = "$GPGSV";
            mVisibleSatellites = new sSat[12];
            mSatellitesInView = 0;
            mTotalMessages = 0;
            mSequnceNumber = 0;
            mSatIndex = 0;
        }

        public override void Parse(string[] nmea)
        {
            if (!IsHeaderValid(nmea[0]))
            {
                return;
            }

            mTotalMessages = int.Parse(nmea[1]);
            mSequnceNumber = int.Parse(nmea[2]);
            mSatellitesInView = int.Parse(nmea[3]);

            // Reset the sat index if this is a new set of records
            if (mSequnceNumber == 1)
            {
                mSatIndex = 0;
            }

            // Starting at index 4 we need to look for each sat record
            for (int nmeaIndex = 4; nmeaIndex < nmea.Length - 1; )
            {
                mVisibleSatellites[mSatIndex].mID = int.Parse(nmea[nmeaIndex++]);
                mVisibleSatellites[mSatIndex].mElevation_deg = int.Parse(nmea[nmeaIndex++]);
                mVisibleSatellites[mSatIndex].mAzimuth_deg = int.Parse(nmea[nmeaIndex++]);
                int.TryParse(nmea[nmeaIndex++], out mVisibleSatellites[mSatIndex].mSNR);    // could be NULL so TryParse()
                mSatIndex++;
            }

            ParseChecksum(nmea[nmea.Length - 1]);
        }

        /// <summary>
        /// Returns the total number of satellite messages to be received.
        /// </summary>
        public int TotalMessages
        {
            get { return mTotalMessages; }
        }

        /// <summary>
        /// Returns the current sequence number in the message queue.
        /// </summary>
        public int SequenceNumber
        {
            get { return mSequnceNumber; }
        }

        /// <summary>
        /// Returns an array of sSat objects.  One for each visible satelliite.
        /// </summary>
        public sSat[] VisibleSatellites
        {
            get { return mVisibleSatellites; }
        }

        /// <summary>
        /// Returns the number of satellites currently in view.
        /// </summary>
        public int SatellitesInView
        {
            get { return mSatellitesInView; }
        }

        private sSat[] mVisibleSatellites;
        private int mSatellitesInView;
        private int mTotalMessages;
        private int mSequnceNumber;
        private int mSatIndex;
    }
}
