﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NMEA_Utils
{
    /// <summary>
    /// A class used as a single entry point for decoding NMEA sentences.
    /// </summary>
    public class NMEA_Handler
    {
        public NMEA_Handler()
        {
            // Create our NMEA sentence parsers.
            mGGA = new NMEA_SentenceGGA();
            mGLL = new NMEA_SentenceGLL();
            mGSA = new NMEA_SentenceGSA();
            mGSV = new NMEA_SentenceGSV();
            mRMC = new NMEA_SentenceRMC();
            mVTG = new NMEA_SentenceVTG();
        }

        /// <summary>
        /// Method to call when subscribers need to be told about an Error.
        /// </summary>
        /// <param name="message"></param>
        public void LogError(string message)
        {
            ErrorHandler.Instance().LogError(message);
        }

        /// <summary>
        /// Takes a NMEA sentence and passes it to the appropriate handler
        /// for decoding and storage.
        /// </summary>
        /// <param name="sentence"></param>
        public void ParseSentence(string sentence)
        {
            char[] chars = { ',', '*' };
            string[] data = sentence.Split(chars);

            try
            {
                switch (data[0])
                {
                    case "$GPGGA": mGGA.Parse(data); break;
                    case "$GPGLL": mGLL.Parse(data); break;
                    case "$GPGSA": mGSA.Parse(data); break;
                    case "$GPGSV": mGSV.Parse(data); break;
                    case "$GPRMC": mRMC.Parse(data); break;
                    case "$GPVTG": mVTG.Parse(data); break;
                    default:
                        LogError("NMEA_Handler.ParseSentence(): Unknown NMEA header!");
                        break;
                }
            }
            catch (Exception e)
            {
                LogError("NMEA_Handler.ParseSentence(): " + e.Message + " When parsing " + data);
            }
        }

        /// <summary>
        /// Converts the given value in ddmm.mmmmn format.
        /// </summary>
        /// <param name="input"></param>
        /// <returns>Returns the given value in DD.dddd° N</returns>
        public static string ParseLatitudeD(string input)
        {
            double latitude_deg = 0.0;

            latitude_deg = double.Parse(input.Substring(0, 2));
            latitude_deg += double.Parse(input.Substring(2, 7)) / 60.0;

            return latitude_deg + "° " + input[input.Length - 1];
        }

        /// <summary>
        /// Converts the given value in dddmm.mmmme format.
        /// </summary>
        /// <param name="input"></param>
        /// <returns>Returns the given value in DDD.dddd° E</returns>
        public static string ParseLongitudeD(string input)
        {
            double longitude_deg = 0.0;

            longitude_deg = double.Parse(input.Substring(0, 3));
            longitude_deg += double.Parse(input.Substring(3, 7)) / 60.0;

            return longitude_deg + "° " + input[input.Length - 1];
        }

        /// <summary>
        /// Converts the given value in ddmm.mmmmn format.
        /// </summary>
        /// <param name="input"></param>
        /// <returns>Returns the given value in DD° MM.mmmm′ N</returns>
        public static string ParseLatitudeDM(string input)
        {
            return input.Substring(0, 2) + "° " +
                input.Substring(2, 7) + "′ " + 
                input[input.Length-1];
        }

        /// <summary>
        /// Converts the given value in dddmm.mmmme format.
        /// </summary>
        /// <param name="input"></param>
        /// <returns>Returns the given value in DDD° MM.mmmm′ E</returns>
        public static string ParseLongitudeDM(string input)
        {
            return input.Substring(0, 3) + "° " +
                input.Substring(3, 7) + "′ " +
                input[input.Length-1];
        }

        /// <summary>
        /// Converts the given value in ddmm.mmmmn format.
        /// </summary>
        /// <param name="input"></param>
        /// <returns>Returns the given value in DD° MM′ SS.ss″ E</returns>
        public static string ParseLatitudeDMS(string input)
        {
            double seconds = double.Parse(input.Substring(4, 5)) * 60.0;

            return input.Substring(0, 2) + "° " +
                input.Substring(2, 2) + "′ " +
                seconds.ToString() + "″ " + input[input.Length - 1];
        }

        /// <summary>
        /// Converts the given value in dddmm.mmmmn format.
        /// </summary>
        /// <param name="input"></param>
        /// <returns>Returns the given value in DDD° MM′ SS.ss″ E</returns>
        public static string ParseLongitudeDMS(string input)
        {
            double seconds = double.Parse(input.Substring(5, 5)) * 60.0;

            return input.Substring(0, 3) + "° " +
                input.Substring(3, 2) + "′ " +
                seconds.ToString() + "″ " + input[input.Length - 1];
        }

        /// <summary>
        /// Method turns a Latitude in ddmm.mmmm into decimal d.dddd
        /// </summary>
        /// <param name="input"></param>
        /// <param name="ns"></param>
        /// <returns>Returns a decimal representation of the input latitude string.</returns>
        public static double ParseLatitudeDeg(string input, string ns)
        {
            double latitude_deg = 0.0;

            latitude_deg = double.Parse(input.Substring(0, 2));
            latitude_deg += double.Parse(input.Substring(2, 7)) / 60.0;

            if (ns.Equals("S"))
            {
                latitude_deg *= -1.0;
            }

            return latitude_deg;
        }

        /// <summary>
        /// Method turns a Longitude in ddmm.mmmm into decimal d.dddd
        /// </summary>
        /// <param name="input"></param>
        /// <param name="ew"></param>
        /// <returns>Returns a decimal representation of the input longitude string.</returns>
        public static double ParseLongitudeDeg(string input, string ew)
        {
            double longitude_deg = 0.0;

            longitude_deg = double.Parse(input.Substring(0, 3));
            longitude_deg += double.Parse(input.Substring(3, 7)) / 60.0;

            if (ew.Equals("W"))
            {
                longitude_deg *= -1.0;
            }

            return longitude_deg;
        }

        /// <summary>
        /// Converts UTC time from the hhmmss.sss format to the 7:34:42Z format compatible with DateTime methods.
        /// </summary>
        /// <param name="utcTime"></param>
        /// <returns>A DateTime compatible string representation of UTC time.</returns>
        public static string ParseUtcTime(string utcTime)
        {
            return utcTime.Substring(0,2) + ":" + utcTime.Substring(2,2) + ":" + utcTime.Substring(4,2) + "Z";
        }

        /// <summary>
        /// Converts UTC date from the ddmmyy format to the 2008-05-01 format compatible with DateTime methods.
        /// NOTE: Prependes "20" to the year. i.e. "12" becomes "2012", etc.
        /// </summary>
        /// <param name="utcTime"></param>
        /// <returns>A DateTime compatible string representation of the UTC date.</returns>
        public static string ParseUtcDate(string utcTime)
        {
            return "20" + utcTime.Substring(4,2) + "-" + utcTime.Substring(2,2) + "-" + utcTime.Substring(0,2);
        }

        /// <summary>
        /// Converts the coded input parameter to a mode indicator string.
        /// </summary>
        /// <param name="input"></param>
        /// <returns>The Mode Indicator string.</returns>
        public static string ParseModeIndicator(string input)
        {
            string modeIndicator;

            switch (input)
            {
                case "N": modeIndicator = "Data not valid"; break;
                case "A": modeIndicator = "Autonomous"; break;
                case "D": modeIndicator = "Differential"; break;
                case "E": modeIndicator = "Dead Reckoning"; break;
                case "M": modeIndicator = "Manual Input"; break;
                case "S": modeIndicator = "Simulator"; break;
                default: modeIndicator = "Invalid Code"; break;
            }

            return modeIndicator;
        }

        public NMEA_SentenceGGA GGA
        {
            get { return mGGA; }
        }

        public NMEA_SentenceGLL GLL
        {
            get { return mGLL; }
        }

        public NMEA_SentenceGSA GSA
        {
            get { return mGSA; }
        }

        public NMEA_SentenceGSV GSV
        {
            get { return mGSV; }
        }

        public NMEA_SentenceRMC RMC
        {
            get { return mRMC; }
        }

        public NMEA_SentenceVTG VTG
        {
            get { return mVTG; }
        }

        private NMEA_SentenceGGA mGGA;
        private NMEA_SentenceGLL mGLL;
        private NMEA_SentenceGSA mGSA;
        private NMEA_SentenceGSV mGSV;
        private NMEA_SentenceRMC mRMC;
        private NMEA_SentenceVTG mVTG;
    }
}
