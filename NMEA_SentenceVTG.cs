﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NMEA_Utils
{
    /// <summary>
    ///     VTG – Course Over Ground and Ground Speed
    ///         The Actual course and speed relative to the ground.
    ///     Structure:
    ///         GPVTG,x.x,T,,M,x.x,N,x.x,K,a*hh<CR><LF>
    /// </summary>
    public class NMEA_SentenceVTG : NMEA_Sentence
    {
        public NMEA_SentenceVTG()
        {
            mHeader = "$GPVTG";
            mTrueCourse_deg = 0.0;
            mMagneticCourse_deg = 0.0;
            mGroundSpeed_knots = 0.0;
            mGroundSpeed_kph = 0.0;
            mModeIndicator = "N/A";
        }

        public override void Parse(string[] nmea)
        {
            if (!IsHeaderValid(nmea[0]))
            {
                return;
            }

            mTrueCourse_deg = double.Parse(nmea[1]);
            //nmea[2] == "T";
            double.TryParse(nmea[3], out mMagneticCourse_deg);
            //nmea[4] == "M";
            mGroundSpeed_knots = double.Parse(nmea[5]);
            //name[6] == "N"; // for knots
            mGroundSpeed_kph = double.Parse(nmea[7]);
            //nmea[8] == "K"; // for kph
            mModeIndicator = NMEA_Handler.ParseModeIndicator(nmea[9]);

            ParseChecksum(nmea[10]);
        }

        /// <summary>
        /// Returns true course in degrees.
        /// </summary>
        public double TrueCourse_Deg
        {
            get { return mTrueCourse_deg; }
        }

        /// <summary>
        /// Returns magnetic course in degrees.
        /// </summary>
        public double MagneticCourse_Deg
        {
            get { return mMagneticCourse_deg; }
        }

        /// <summary>
        /// Returns ground speed in knots.
        /// </summary>
        public double GroundSpeed_Knots
        {
            get { return mGroundSpeed_knots; }
        }

        /// <summary>
        /// Returns ground speed in kilometers per hour.
        /// </summary>
        public double GroundSpeed_Kph
        {
            get { return mGroundSpeed_kph; }
        }

        /// <summary>
        /// Returns the GPS mode.  Possible values are:
        ///     Not valid
        ///     Autonomous mode
        ///     Differential mode
        ///     Estimated (dead reckoning) mode
        ///     Manual input mode
        ///     Simulator mode
        /// </summary>
        public string ModeIndicator
        {
            get { return mModeIndicator; }
        }

        private double mTrueCourse_deg;
        private double mMagneticCourse_deg;
        private double mGroundSpeed_knots;
        private double mGroundSpeed_kph;
        private string mModeIndicator;
    }
}
