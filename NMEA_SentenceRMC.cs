﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NMEA_Utils
{
    /// <summary>
    /// RMC – Recommended Minimum Specific GNSS Data
    ///     Time, date, position, course and speed data provided by a GNSS navigation receiver.
    /// Structure:
    ///     $GPRMC,hhmmss.sss,A,dddmm.mmmm,a,dddmm.mmmm,a,x.x,x.x,ddmmyy,,,a*hh<CR><LF>
    /// </summary>
    public class NMEA_SentenceRMC : NMEA_SentenceWithPositionAndUtcTime
    {
        public NMEA_SentenceRMC()
        {
            mHeader = "$GPRMC";
            mStatus = "N/A";
            mGroundSpeed_knots = 0.0;
            mTrackAngle_deg = 0.0;
            mUtcDateOfPosFix = "2001-01-01";
            mMagVar_deg = 0.0;
            mModeIndicator = "N/A";
        }

        public override void Parse(string[] nmea)
        {
            if (!IsHeaderValid(nmea[0]))
            {
                return;
            }

            mUtcTime = NMEA_Handler.ParseUtcTime(nmea[1]);
            mStatus = ParseStatus(nmea[2]);
            mLatitudeD = NMEA_Handler.ParseLatitudeD(nmea[3] + nmea[4]);
            mLongitudeD = NMEA_Handler.ParseLongitudeD(nmea[5] + nmea[6]);
            mLatitudeDM = NMEA_Handler.ParseLatitudeDM(nmea[3] + nmea[4]);
            mLongitudeDM = NMEA_Handler.ParseLongitudeDM(nmea[5] + nmea[6]);
            mLatitudeDMS = NMEA_Handler.ParseLatitudeDMS(nmea[3] + nmea[4]);
            mLongitudeDMS = NMEA_Handler.ParseLongitudeDMS(nmea[5] + nmea[6]);
            mLatitude_deg = NMEA_Handler.ParseLatitudeDeg(nmea[3], nmea[4]);
            mLongitude_deg = NMEA_Handler.ParseLongitudeDeg(nmea[5], nmea[6]);
            mGroundSpeed_knots = double.Parse(nmea[7]);
            mTrackAngle_deg = double.Parse(nmea[8]);
            mUtcDateOfPosFix = NMEA_Handler.ParseUtcDate(nmea[9]);

            double.TryParse(nmea[10], out mMagVar_deg); // Could be NULL

            if (nmea[11].Equals("E"))
            {
                mMagVar_deg *= -1.0;
            }

            mModeIndicator = NMEA_Handler.ParseModeIndicator(nmea[12]);
            ParseChecksum(nmea[13]);
        }

        private string ParseStatus(string status)
        {
            switch (status)
            {
                case "V": mStatus = "Navigation receiver warning"; break;
                case "A": mStatus = "Data Valid"; break;
                default: mStatus = "Invalid Code"; break;
            }

            return mStatus;
        }

        /// <summary>
        /// Returns the current GPS status.  Possible values are:
        ///     Navigation receiver warning
        ///     Data Valid
        /// </summary>
        public string Status
        {
            get { return mStatus; }
        }

        /// <summary>
        /// Returns ground speed in knots.
        /// </summary>
        public double GroundSpeed_Knots
        {
            get { return mGroundSpeed_knots; }
        }

        /// <summary>
        /// Returns track angle in degrees. 0.0 - 359.9
        /// </summary>
        public double TrackAngle_Deg
        {
            get { return mTrackAngle_deg; }
        }

        /// <summary>
        /// Returns the UTC Date.
        /// </summary>
        public string UtcDateOfPosFix
        {
            get { return mUtcDateOfPosFix; }
        }

        /// <summary>
        /// Returns the magnetic variation in degrees.
        /// </summary>
        public double MagVar_Deg
        {
            get { return mMagVar_deg; }
        }

        /// <summary>
        /// Returns the GPS mode.  Possible values are:
        ///     Data not valid
        ///     Autonomous mode
        ///     Differential mode
        ///     Estimated (dead reckoning) mode
        ///     Manual input mode
        ///     Simulator mode
        /// </summary>
        public string ModeIndicator
        {
            get { return mModeIndicator; }
        }

        private string mStatus;
        private double mGroundSpeed_knots;
        private double mTrackAngle_deg;
        private string mUtcDateOfPosFix;
        private double mMagVar_deg;
        private string mModeIndicator;
    }
}
