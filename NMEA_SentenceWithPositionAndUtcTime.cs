﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NMEA_Utils
{
    /// <summary>
    /// Base class for NMEA sentences that have position information latitude 
    /// and longitude.  Provides properties for accessing position info in 
    /// multiple formats.  Available formats: Degrees, DM, DMS.
    /// </summary>
    public class NMEA_SentenceWithPositionAndUtcTime : NMEA_Sentence
    {
        public NMEA_SentenceWithPositionAndUtcTime()
        {
            mUtcTime = "00:00:00";
            mLatitudeD = "00.00° N";
            mLongitudeD = "000.00° E";
            mLatitudeDM = "00° 00.00′ N";
            mLongitudeDM = "000° 00.00′ E";
            mLatitudeDMS = "00° 00′ 00.00″ N";
            mLongitudeDMS = "000° 00′ 00.00″ E";
            mLatitude_deg = 0.0;
            mLongitude_deg = 0.0;
        }

        /// <summary>
        /// This method is inherited from the base class but is not used.
        /// </summary>
        /// <param name="nmea"></param>
        public override void Parse(string[] nmea)
        {
            // Do nothing...
        }

        /// <summary>
        /// Returns the UTC Time in '07:34:42Z' format.
        /// </summary>
        public string UtcTime
        {
            get { return mUtcTime; }
        }

        /// <summary>
        /// Returns latitude in DD.dddd format.
        /// </summary>
        public string LatitudeD
        {
            get { return mLatitudeD; }
        }

        /// <summary>
        /// Returns longitude in DDD.dddd format.
        /// </summary>
        public string LongitudeD
        {
            get { return mLongitudeD; }
        }

        /// <summary>
        /// Returns latitude in DD MM.mmmm format.
        /// </summary>
        public string LatitudeDM
        {
            get { return mLatitudeDM; }
        }

        /// <summary>
        /// Returns longitude in DDD MM.mmmm format.
        /// </summary>
        public string LongitudeDM
        {
            get { return mLongitudeDM; }
        }

        /// <summary>
        /// Returns latitude in DD MM SS.ssss format.
        /// </summary>
        public string LatitudeDMS
        {
            get { return mLatitudeDMS; }
        }

        /// <summary>
        /// Returns longitude in DDD MM SS.ssss format.
        /// </summary>
        public string LongitudeDMS
        {
            get { return mLongitudeDMS; }
        }

        /// <summary>
        /// Returns latitude in degrees.  Negative is south of the equator.
        /// </summary>
        public double Latitude_Deg
        {
            get { return mLatitude_deg; }
        }

        /// <summary>
        /// Returns longitude in degrees.  Negative is west of the prime meridian.
        /// </summary>
        public double Longitude_Deg
        {
            get { return mLongitude_deg; }
        }

        protected string mUtcTime;
        protected string mLatitudeD;
        protected string mLongitudeD;
        protected string mLatitudeDM;
        protected string mLongitudeDM;
        protected string mLatitudeDMS;
        protected string mLongitudeDMS;
        protected double mLatitude_deg;
        protected double mLongitude_deg;
    }
}
