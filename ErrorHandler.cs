﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;

namespace NMEA_Utils
{
    /// <summary>
    /// ErrorLogEventArgs class definition used when an error is logged by any 
    /// NMEA sentence owned by the NMEA_Handler.
    /// </summary>
    public class NMEA_ErrorEventArgs : EventArgs
    {
        public NMEA_ErrorEventArgs(string s)
        {
            message = s;
        }
        private string message;

        public string Message
        {
            get { return message; }
            set { message = value; }
        }
    }

    /// <summary>
    /// Class that handles logging errors for the NMEA Utils project.
    /// </summary>
    public class ErrorHandler
    {
        /// <summary>
        /// Creates an instance of the class and returns it.
        /// </summary>
        /// <returns>An instance of this class.</returns>
        public static ErrorHandler Instance()
        {
            if (theInstance == null)
            {
                theInstance = new ErrorHandler();
            }

            return theInstance;
        }

        /// <summary>
        /// Method to call when subscribers need to be told about an Error.
        /// </summary>
        /// <param name="message"></param>
        public void LogError(string message)
        {
            NMEA_ErrorEventArgs e = new NMEA_ErrorEventArgs(message);
            EventHandler<NMEA_ErrorEventArgs> handler = NMEA_ErrorEvent;

            if (handler != null)
            {
                e.Message = DateTime.Now.ToString() + ": " + e.Message;
                handler(this, e);
            }
        }

        private static ErrorHandler theInstance;
        public event EventHandler<NMEA_ErrorEventArgs> NMEA_ErrorEvent;
    }
}
